#include <vector>
#include <iostream>
int get_input(size_t &W1, size_t &W2, std::vector<int> &w, std::vector<int> &v1, std::vector<int> &v2)
{
    size_t n = 0;
    if(! (std::cin >> n)) return 1;
    if(! (std::cin >> W1)) return 1;
    if(! (std::cin >> W2)) return 1;
    w.resize(n);
    for(auto &i : w) std::cin >> i;
    v1.resize(n);
    for(auto &i : v1) std::cin >> i;
    v2.resize(n);
    for(auto &i : v2) std::cin >> i;
    if(!std::cin) return 1;
    return 0;
}


typedef std::vector<std::vector<std::vector<int>>> Vec3Int;

void vec3int_init(Vec3Int &m, size_t l1, size_t l2, size_t l3)
{
    m.resize(l1);
    for(auto &i : m)
    {
        i.resize(l2);
        for(auto &j : i)
            j.resize(l3);
    }
}

int main()
{
    size_t W1,W2;
    std::vector<int> w,v1,v2;
    if(get_input(W1,W2,w,v1,v2)) return 1;

    //
    Vec3Int M;
    vec3int_init(M, w.size()+1, W1+1, W2+1);

    // If no item, the total value is 0
    for(auto &i: M[0] )
        for(auto &j: i )
            j = 0;

    for(size_t i = 1; i < M.size(); ++i)
        for(size_t w1 = 0; w1 <= W1; ++w1)
            for(size_t w2 = 0; w2 <= W2; ++w2)
            {
                M[i][w1][w2] = M[i-1][w1][w2];
                int v_use1 = w[i-1] > w1 ? -1 : M[i-1][w1-w[i-1]][w2] + v1[i-1];
                int v_use2 = w[i-1] > w2 ? -1 : M[i-1][w1][w2-w[i-1]] + v2[i-1];
                if( v_use1 > M[i][w1][w2] )   M[i][w1][w2] = v_use1;
                if( v_use2 > M[i][w1][w2] )   M[i][w1][w2] = v_use2;
            }

    std::vector<size_t> S1 = {};
    std::vector<size_t> S2 = {};
    S1.reserve(w.size());
    S2.reserve(w.size());

    for(size_t i = M.size()-1, w1 = W1, w2 = W2; i>0; --i)
    {
        if( w1>=w[i-1] && M[i][w1][w2] == v1[i-1] + M[i-1][w1-w[i-1]][w2] ){
            S1.push_back(i);
            w1 -= w[i-1];
        } else
        if( w2>=w[i-1] && M[i][w1][w2] == v2[i-1] + M[i-1][w1][w2-w[i-1]]){
            S2.push_back(i);
            w2 -= w[i-1];
        }
    }

    std::cout << M[w.size()][W1][W2] << std::endl;
    for(auto it = S1.rbegin(); it != S1.rend(); ++it)
        std::cout << *it << ' ';
    std::cout << std::endl;
    for(auto it = S2.rbegin(); it != S2.rend(); ++it)
        std::cout << *it << ' ';
    std::cout << std::endl;
}

