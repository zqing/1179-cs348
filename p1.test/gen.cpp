#include <random>
#include <iostream>
#include <unordered_set>
#include <utility>

using namespace std;

int main() {
    unordered_set<long long> cs;
    unordered_set<int> is;
    random_device r;
    mt19937 mt(r());
    int max;
    long long h;
    int id, x, y;
    cin >> max;
    cout << max << endl;
    for (int i = 0; i < max; ++i) {
	do {
	    id = mt() % 1048576;
	} while (is.find(id) != is.end());
	is.insert(id);
	do {
	    x = mt() % 1024;
	    y = mt() % 1024;
	    h = x * 1024 + y;
	} while (cs.find(h) != cs.end());
	cs.insert(h);
        cout << id << " " << x << " " << y << endl;
    }
}
