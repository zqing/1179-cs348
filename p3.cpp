#include <vector>
#include <iostream>
#include <algorithm>

struct Edge { unsigned u,v; int weight; };
typedef std::vector<int> IntVector;
//typedef std::vector<IntVector> IntMatrix;
typedef std::vector<Edge> EdgeVector;

std::istream &operator >> (std::istream &is, Edge & e){
    is >> e.u >> e.v >> e.weight;
    if(e.u > e.v){
        auto tmp = e.u;
        e.u = e.v;
        e.v = tmp;
    }
    e.u --;
    e.v --;
    return is;
}

std::ostream &operator << (std::ostream &os, Edge & e){
    os << e.u+1 << ' ' << e.v+1 << std::endl;
    return os;
}

/* negative indicates infinity*/
int min(int a, int b){
    if(a < 0) return b;
    if(b < 0) return a;
    return a < b? a : b;
}

int add(int a, int b){
    if(a < 0 || b < 0) return -1;
    return a + b;
}

int main(){
    //// Get Input
    unsigned n,m;
    double t;
    std::cin >> n >> m >> t;

    EdgeVector G (m);
    for(auto &e : G)  std::cin >> e;

    //// Algorithm

    std::sort(G.begin(),G.end(), [](Edge &e1,Edge &e2)->bool { return e1.weight <  e2.weight; });

    EdgeVector H;

    for(auto &e : G){
        unsigned dist /* = shortest path from e.u to e.v in H */;

        {
            /* A Bellman-Ford Implementation */
            IntVector D(n, -1);
            D[e.u] = 0;
            for (auto i: D) {
                for (auto &e2 : H) {
                    D[e2.v] = min(D[e2.v], add(D[e2.u], e2.weight));
                    D[e2.u] = min(D[e2.u], add(D[e2.v], e2.weight));
                }
                (void) i;
            }
            dist = (unsigned) (D[e.v] < 0 ? -1 : D[e.v]);
        }

        if(t * e.weight <  dist) H.push_back(e);
    }

    std::sort(H.begin(),H.end(),[](Edge &e1,Edge &e2)->bool { return e1.u!=e2.u ? e1.u<e2.u : e1.v<e2.v; });

    std::cout << H.size() << std::endl;
    int sum = 0;
    for(auto &e:H) sum += e.weight;
    std::cout << sum << std::endl;
    for(auto &e:H)
        std::cout << e;


    return 0;
}


