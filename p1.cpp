#include <vector>

typedef struct {
    int id, x, y, load;
} Radar;

typedef std::vector<Radar *> RadarList;

bool xless (Radar *r1, Radar *r2){ return r1->x != r2->x ? r1->x < r2->x : r1->y < r2->y; }

bool yless (Radar *r1, Radar *r2){ return r1->y != r2->y ? r1->y < r2->y : r1->x < r2->x; }

#define RL_PRINT(list) \
    { std::cerr << #list << std::endl; \
    for(auto i: list) std::cerr << i->id << ' ' << i->x << ' ' << i->y << std::endl; }

#include <iostream>
#include <algorithm>

#ifdef USE_DC
void set_load(const RadarList &X, const RadarList &Y)
{
    if( X.size() <= 1 ) return;

    auto mid = X.begin() + ( X.size() / 2 );
    RadarList X1( X.begin(), mid );
    RadarList X2( mid, X.end() );
    RadarList Y1 = {};
    RadarList Y2 = {};

    for (auto i : Y)
    {
        if(xless(i, *mid)) Y1.push_back(i);
        else Y2.push_back(i);
    }

    set_load(X1,Y1);
    set_load(X2,Y2);

    while ( ! Y2.empty() )
    {
        while ( !Y1.empty() && yless(Y2.back(), Y1.back()) ) Y1.pop_back();
        Y2.back()->load += Y1.size();
        Y2.pop_back();
    }
}
#endif

int main(){
    size_t sz = 0;
    RadarList radars;
#ifdef USE_DC
    RadarList xsorted;
    RadarList ysorted;
#endif

    std::cin >> sz;
    while ( sz > 0 )
    {
        int id, x, y;
        std::cin >> id >> x >> y;
        auto *radar = new Radar{id, x, y, 0};
        radars.push_back(radar);
#ifdef USE_DC
        xsorted.push_back(radar);
        ysorted.push_back(radar);
#endif
        -- sz;
    }

    std::sort(radars.begin(),radars.end(), [](Radar *r1, Radar *r2)->bool{return r1->id < r2->id;} );

#ifdef USE_DC
    std::sort(xsorted.begin(),xsorted.end(), xless);
    std::sort(ysorted.begin(),ysorted.end(), yless);
    set_load(xsorted, ysorted);
#else

    for(auto i: radars)
    {
        i -> load = 0;
        for(auto j: radars)
        {
            if (j->x <= i->x && j->y <= i->y ) ++ i->load;
        }
        -- i->load;
    }
#endif

    for(auto i: radars)
    {
        std::cout << i->id << ' ' << i->load << std::endl;
    }

    return 0;
}